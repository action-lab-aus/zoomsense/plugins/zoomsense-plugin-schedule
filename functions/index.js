const functions = require('firebase-functions');
// const environment = functions.config()[process.env.GCLOUD_PROJECT];
const { defineString } = require('firebase-functions/params');
googleClientId = defineString('OAUTH_CLIENT_ID');
googleRedirectUrl = defineString('OAUTH_REDIRECT_URI');

const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.database();
const { google } = require('googleapis');
// const CALENDAR_ID = environment.google.calendar_id;
const moment = require('moment');

exports.scheduleNextSession = functions
  .runWith({ secrets: ['CALENDAR_ID', 'GOOGLE_CLIENT_SECRET'] })
  .https.onCall(async (data, context) => {
    // if (!context.auth.uid) throw new functions.https.HttpsError('unauthenticated', `Unauthenticated user...`);

    const { startTime, endTime, coachUid, patientUid, template, topic, type, reminder } = data;
    functions.logger.info('scheduleNextSession Data: ', data);

    if (!startTime || !endTime || !patientUid || !template || !topic || !type)
      throw new functions.https.HttpsError('invalid-argument', 'Invalid argument');

    // Get patient's email from RTDB
    const patient = (await db.ref(`patients/${patientUid}`).once('value')).val();
    const patientEmail = patient.email;

    // Get coach's email from RTDB
    const coach = await admin.auth().getUser(context.auth.uid);
    const coachEmail = coach.email;

    // Add OAuth to to Google client
    const oauth2Client = await getOAuthClient();
    const calendar = google.calendar('v3');
    google.options({ auth: oauth2Client });

    // Construct the description string
    const description = `Coach: ${coachEmail}\nClient UID: ${patientUid}\nTemplate: ${template}`;

    // Create Google Calendar event for the session
    let newmeeting = await calendar.events.insert({
      calendarId: process.env.CALENDAR_ID,
      sendUpdates: 'none',
      sendNotifications: false,
      requestBody: {
        summary: `${topic}`,
        description: description,
        attendees: [{ email: coachEmail }, { email: patientEmail }],
        start: {
          dateTime: startTime,
          timeZone: 'Australia/Melbourne',
        },
        end: {
          dateTime: endTime,
          timeZone: 'Australia/Melbourne',
        },
      },
    });

    // Push reminders to the queue
    if (reminder) {
      let when = moment(startTime).subtract(reminder, 'days');
      // functions.logger.info('When:', when);

      // reminderDate.setDate(reminderDate.getDate() - reminder.split(' ')[0]);
      // console.log(newmeeting);
      // functions.logger.info('Created Event: ', newmeeting.data);
      const meetingid = newmeeting.data.id;
      const patientName = (await db.ref(`patients/${patientUid}`).once('value')).val().name;
      const reminderObj = {
        coachEmail,
        patientName,
        patientEmail,
        coachUid,
        topic,
        when: when.valueOf(),
        offset: reminder,
      };

      await db.ref('reminders').child(meetingid).set(reminderObj);
    }
  });

exports.updateMeetingReminder = functions.database.ref('/meetings/{uid}/{mid}').onUpdate(async (snapshot, data) => {
  // console.log(snapshot);
  let meeting = snapshot.after.val();
  let reminder = (await db.ref(`reminders/${data.params.mid}`).once('value')).val();
  let when = moment(meeting.startTime).subtract(reminder.offset, 'days');
  //update reminder:
  await db.ref(`reminders/${data.params.mid}`).update({
    when: when.valueOf(),
  });
});

// /**
//  * Send SendGrid notification emails with optional attachments using Dynamic Templates
//  *
//  * @param email Target email address
//  * @param dynamicId SendGrid dynamic template id
//  * @param params SendGrid Handle Bar parameters (e.g. sessionId, coachEmail, curationUrl)
//  * @param attachments An array of email attachmenmts
//  *
//  * @returns SendGrid emailing client response
//  */
// async function sendSendGridEmail(email, dynamicId, params, attachments) {
//   functions.logger.info('email: ', email);
//   functions.logger.info('dynamicId: ', dynamicId);
//   functions.logger.info('params: ', params);

//   let msg = {
//     to: email,
//     from: {
//       email: SENDGRID_SENDER,
//       name: SENDGRID_NAME,
//     },
//     templateId: dynamicId,
//     dynamic_template_data: params,
//   };

//   if (attachments) msg.attachments = attachments;

//   const response = await sgMail.send(msg);
//   functions.logger.info(response[0].statusCode);
//   return response;
// }

async function getOAuthClient() {
  // Get refresh token from RTDB
  const snapshot = await db.ref('credentials').once('value');
  const refreshToken = snapshot.val().refresh_token;

  // Initialize Google OAuth client
  const oauth2Client = new google.auth.OAuth2(
    googleClientId.value(),
    process.env.GOOGLE_CLIENT_SECRET,
    googleRedirectUrl.value(),
  );

  // Get access token with refresh token & reset the token in Firestore
  oauth2Client.credentials.refresh_token = refreshToken;
  const response = await oauth2Client.getAccessToken();
  const credentials = response.res.data;
  oauth2Client.setCredentials(credentials);
  await db.ref('credentials').update(credentials);
  return oauth2Client;
}
